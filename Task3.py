#!/usr/bin/env python3
"""Task 3 from page 82"""

my_third_list = [3.14, 5.31, 7.42, -5.75, 9.99]

for item in my_third_list:
    print(item)
